import smtplib
from email.message import EmailMessage

from celery import Celery
from config import SMTP_USER, SMTP_PASSWORD

SMTP_HOST = "smtp.gmail.com"
SMTP_PORT = 465

celery = Celery('tasks', broker='redis://localhost:6379')


def get_email_template_dashboard(username: str, email: str):
    EmailFunc = EmailMessage()
    EmailFunc['Subject'] = 'Натрейдил'
    EmailFunc['From'] = SMTP_USER
    EmailFunc['To'] = email

    EmailFunc.set_content(
        '<div>'
        f'<h1 style="color: red;">Здравствуйте, {username}, а вот и ваш отчет. Зацените</h1>'
        '<img src="https://static.vecteezy.com/system/resources/previews/008/295/031/original/custom-relationship'
        '-management-dashboard-ui-design-template-suitable-designing-application-for-android-and-ios-clean-style-app'
        '-mobile-free-vector.jpg" width="600">'
        '<div>',
        subtype='html'
    )
    return EmailFunc


@celery.task
def send_email_report_dashboard(username: str, email: str):
    email1 = get_email_template_dashboard(username, email)
    with smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT) as server:
        server.login(SMTP_USER, SMTP_PASSWORD)
        server.send_message(email1)
